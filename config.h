/************************************************/
/*软件版本：0030
/*硬件版本：YDCXZZ-V0.3 
/*软件发布日期：2010.09.25
/*硬件发布日期：2010.09.15
/*最终修改日期：2010.09.25
/************************************************/
#ifndef CONFIG_H
#define CONFIG_H

#include"stc8051.h"
#include"intrins.h" 

#define uchar unsigned char
#define uint unsigned int

#define	VERL 0x63			//版本号低位
#define	VERH 0x33			//版本号高位

/******************管脚定义区**************************/
#define DATA P0     //LCD管脚定义
sbit LCD_RST = P1^4;
sbit E1 = P1^3;
sbit E2 = P1^2;
sbit RW = P1^1;
sbit A0 = P1^0;
sbit B_LED = P4^6;

sbit EE_WP = P2^5;		 //EEPROM的管脚
sbit EE_SDA = P2^6;
sbit EE_SCL = P2^7;

sbit ALARM = P3^5;		 //蜂鸣器 低电平有效
sbit COM_LED = P3^6;	 //通讯灯 高电平有效
sbit A_LED = P3^7;		 //报警灯 高电平有效
sbit KEYDOWN = P2^2;	 //按键1 低电平触发
sbit KEYUP = P2^1;		 //按键2 低电平触发
sbit KEYCANC= P2^0;		 //按键3 低电平触发




/************LCD宏定义区*******************/
#define LCD_DISPLAYON 0xaf      //af 开显示,
#define LCD_DISPLAYOFF 0xae      //ae 关显示,
#define LCD_DRIVERON 0xa5      //a5 静态显示驱动
#define LCD_DRIVEROFF 0xa4      //a4正常驱动
#define LCD_DUTY16 0xa8          //占空比1/16
#define LCD_DUTY32 0xa9          //占空比1/32
//
#define LCD_END 0xee          /*关闭“读-修改-写”模式,并把列地址指针
                              恢复到打开“读-修改-写”模式前的位置。*/

#define LCD_RESET 0xe2        /*初始化。① 设置显示初始行为第一行;
                              ②页地址设置为第三页。*/

#define LCD_RWMODE 0xe0       /*“读-修改-写”模式 。执行该指令以后,
                              每执行一次写数据,指令列地址自动加1;
                              但执行读数据指令时列地址不会改变。
                              这个状态一直持续到执行“END”指令。
                              注意：在“读-修改-写”模式下,
                              除列地址设置指令之外,其他指令照常执行。 */
#define LCD_STARTLINE0 0xc0      //设置显示起始行 可以加0-31
#define LCD_PAGE0 0xb8          //页地址设置        可以加0-3
#define LCD_COLUMNADDR0 0x00  //列地址设置     可以加0-60

#define LCD_ON() LCD_Command(LCD_DISPLAYON)
#define LCD_OFF() LCD_Command(LCD_DISPLAYOFF)
#define LCD_StaticON() LCD_Command(LCD_DRIVERON)
#define LCD_StaticOFF() LCD_Command(LCD_DRIVEROFF)
#define LCD_Select16() LCD_Command(LCD_DUTY16)
#define LCD_Select32() LCD_Command(LCD_DUTY32)
#define LCD_StartRW() LCD_Command(LCD_RWMODE)
#define LCD_EndRW() LCD_Command(LCD_END)
#define LCD_Reset() LCD_Command(LCD_RESET)
#define LCD_SetStartLine(i) LCD_Command(LCD_STARTLINE0|i)
#define LCD_SetPage(i) LCD_Command(LCD_PAGE0|i)
#define LCD_SetColumn(i) LCD_Command(LCD_COLUMNADDR0|i)


/************系统常量定义区*******************/

#define ENABLE_ISP 0x81 			//系统工作时钟<24MHz 时，对IAP_CONTR 寄存器设置此值

#define CANCALALARMTIME 0x0a		//警报解除时间
#define	KEYDELAY	0x08			//按键消抖时间
#define	KEYLONG		0x3c			//2S

#define ALARMSTART 	10				//报警开始时间
#define ALARMEND	22				//报警结束时间

#define COUNT 0xff               	// 定义串口接收缓冲区大小 
#define RELOAD 0xfa 			 	//串口波特率的重载数据，波特率为9600
#define SECTOR_NO 0x200			 	//定义内部EEPROM的扇区大小

#define LCD_ONTIME 30			 	//LCD点亮时间 /秒
#define TIMEING	   60			 	//等待数据时间 /秒
#define DATA_TIMR  2			 	//数据有效期 /秒
#define ALARM_TURE	2				//闹铃时长 2S
//#define	ALARM_FAUSE	8				//闹铃间隔时间 8S
#define	ALARM_FAUSE	1798			//闹铃间隔时间 1798S
#define	FEED_DOG	0x3c		 	//喂狗
#define	OFFSET	0x33			 	//数据自身的偏移量
#define ALAEM_CAC	0x55		 	//警报解除BY按键
#define ALAEM_CACJ	0x88		 	//警报解除BY充值

/************EEPROM地址分配表*******************/
#define	LOCAL_ADD			0x0000			   //本机电表地址
#define	LOCAL_PASS			0x0200			   //本机密码
#define	LOCAL_DALARMVALUE	0x0400			   //本机电量告警值
#define	LOCAL_JALARMVALUE	0x0600			   //本机金额告警值

#define	LOCAL_ELEVALUE		0x0800			   //本机费率
#define	LOCAL_ELESTA		0x0a00			   //本机电表状态
#define	LOCAL_NELEVAL		0x0c00			   //本机当前电表表码
#define	LOCAL_NELELEAD		0x0e00			   //本机当前剩余电量
#define	LOCAL_NELELEAJ		0x1000			   //本机当前剩余金额 
#define	LOCAL_BUYCONUT		0x1200			   //本机总购电次数
#define	LOCAL_LASTBUYT		0x1400			   //本机最后一次购电时间
#define	LOCAL_ALLDL			0x1600			   //本机当前结算周期用电
#define	LOCAL_LASTALLDL		0x1800			   //本机上一结算周期累计用电
#define	LOCAL_BUYTAT		0x1a00			   //本机累计购电量
#define	LOCAL_LASTGDJE		0x1c00			   //最后一次购电金额
#define	LOCAL_LJGDJNE		0x1e00			   //累计购电金额
#define	LOCAL_DQJTDJ		0x2000			   //当前阶梯电价

#define	LOCAL_TIMING		0x3a00			   //上一次校时时间
#define LOCAL_SETREG		0x3c00			   //已设置位标志 
#define	LOCAL_MAC			0x3e00			   //本机物理地址 
#define	LOCAL_REG			0x4000			   //警报消除事件记录区	向后有16个扇区被占用
/************协议命令字*******************/
#define	HEAD	0xa8					  	//起始字
#define	END		0x16					  	//结束符
#define CON0	0x00						//功能0：确认/否认
#define CON1	0x01						//功能1：复位
#define CON2	0x02						//功能2：设置信息
#define	CON3	0x03						//功能3：读取设置信息
#define CON4	0x04						//功能4：电表数据
#define CON5	0x05						//功能5：连路测试
#define CON6	0x06						//功能6：读取事件记录

#define S_L_MAC 0x34						 //设置本机物理地址
#define	S_L_ADD	0x35						 //设置电表表号
#define	S_PW	0x36						 //设置密码
#define	S_TIME	0x37						 //设置当前时间
#define	S_A_D	0x38						 //设置电量告警值
#define	S_A_J	0x39						 //设置金额告警值


#define D_FL 	0x34						//费率电价
#define	D_ZT	0x35						//电表状态字
#define	D_N_M	0x36						//当前表码
#define	D_L_D	0x37						//当前剩余电量
#define	D_L_J	0x38						//当前剩余金额
#define	D_T_G	0x39					    //总购电次数
#define D_LAST_G 0x3a					    //最后一次购电时间
#define D_N_TAT	0x3b					    //当前结算累计用电量
#define	D_L_TAT 0x3c					    //上一周期结算累计用电量
#define	D_G_TAT 0x3d					    //累计购电量
#define	D_G_LTJE	0x3e					//最后一次购电金额
#define	D_L_JINE	0x3f					//累计购电金额
#define	D_JTDJ		0x40					//当前阶梯电价

/************按键变量区*******************/
//#define	KADD	1				  //电表地址键值
#define	KYUE	1				  //剩余金额键值
#define	KYDL	2				  //剩余电量键值
#define	KZQS	3				  //周期电量开始键值
#define	KZQE	7				  //周期电量结束键值
#define	KDQS	8				  //当前示数开始键值
#define	KDQE	12				  //当前示数结束键值
#define	KDJS 	13				  //电价开始键值
#define	KDJE	16				  //电价结束键值
#define	KGDC	17				  //购电次数键值
#define	KLGD	18				  //累计购电量键值
#define	KLJJ	19				  //累计购电金额键值
#define	KGDT	20				  //最后一次购电时间键值 
#define	KGDJ	21				  //最后一次购电金额键值
#define	KDQJ	22				  //当前阶梯电价键值
#define	KGXT	23				  //最后一次更新时间键值


/************公共结构体*******************/
struct Time
	{
	uchar msec;
	uchar sec;
	uchar min;
	uchar hour;
	uchar day;
	uchar mon;
	uchar year;
	};
union union_temp16
{
    uint un_temp16;
    uchar un_temp8[2];
};

#ifdef  root
	#define EXT_ 
#else
	#define EXT_  extern
#endif
/******************全局变量定义区**************************/
EXT_ uchar xdata buffer[COUNT];					//接收缓冲区
EXT_ uchar point;            					//接收数据个数指示变量
EXT_ uchar Data_len;							//应接收数据长度
EXT_ uchar xdata Data_tmp[COUNT];				//待处理数据缓存区
EXT_ uchar Key_value;							//按键键值
EXT_ uchar Key0_value;
EXT_ uchar Key_count;							//按键计数器
EXT_ uchar Key1_count;							//按键1计数器
EXT_ uchar Key2_count;
EXT_ uchar Key0_count;
EXT_ uchar Key_upcount;							//按键释放计数器
EXT_ uint  Alarm_count;							//报警计数器
EXT_ uchar Counter;								//计数器。
EXT_ uchar Time_Counter;						//校时计数器
EXT_ uchar Lcd_counter;							//LCD关闭计数器
EXT_ uchar Data_time;							//串口数据有效期

EXT_ struct Time Now_time;						//当前时间
EXT_ union union_temp16 my_unTemp16;

EXT_ bdata bit alarm_flg;					    //报警标志
EXT_ bdata bit alarm_canc;						//报警取消
EXT_ bdata bit alarm_time;						//报警时间
EXT_ bdata bit alarm_time0;						//报警时段
EXT_ bdata bit buffer_flag;						//串口数据可用
EXT_ bdata bit timing_flag;						//校时标志
EXT_ bdata bit lcd_flag;						//LCD状态
EXT_ bdata bit lcd_refur;						//LCD刷新标志
EXT_ bdata bit data_t;							//数据有效标志
EXT_ bdata bit data_start;						//数据起始标志
EXT_ bdata bit mac_t;							//物理地址标志
EXT_ bdata bit key_plus;						//按键加标志
EXT_ bdata bit key_up;							//按键释放

EXT_ bdata bit feed_dog;						//喂狗标志

EXT_ bdata bit reset;							//重启标志

EXT_ bdata bit nece1;							//必要项1
EXT_ bdata bit nece2;							//必要项2
EXT_ bdata bit timeout;							//超时

EXT_ bdata bit linktest;						//链路测试

EXT_ bdata bit set_dj;							//电价设置标志
EXT_ bdata bit set_nowdl;						//当前电量设置标志
EXT_ bdata bit set_leadl;						//剩余电量设置标志
EXT_ bdata bit set_leaje;						//剩余金额设置标志
EXT_ bdata bit set_timegd;						//总购电次数设置标志
EXT_ bdata bit set_lastgd;						//最后一次购电时间标志
EXT_ bdata bit set_alldl;						//总购电量设置标志
EXT_ bdata bit set_alljs;						//当前结算用电
//EXT_ bdata bit set_l1js;						//上一周期结算用电量
EXT_ bdata bit set_lgdt;						//最后一次购电金额
EXT_ bdata bit set_ljgdje;						//累计够电金额
EXT_ bdata bit set_dqjtdj;						//当前阶梯电价


/******************函数定义区**************************/ 
void time_init(void);							//定时器初始化

void UART_init();               				//串口初始化函数
void COM_send(uchar nCount);          		  	//串口发送函数


void delay(uchar nnn);							//软件延迟
void LCD_Command(uchar nCommand);				//发送命令
void LCD_SetRam1(uchar ndata); 					//送显示数据E1
void LCD_SetRam2(uchar ndata); 					//送显示数据E2
void LCD_Init(void);						 	//LCD初始化
void clrscr(void);							 	//LCD清屏
void drawword(uchar liedizhi,uchar layer,uchar wenzi); //显示文字
void drawASCII(uchar liedizhi,uchar layer,uchar wenzi);//显示字母
void LCD_shutdown(void);					 		//关闭LCD
void LCD_open(void);					 		//开启LCD
void LCD_Busy(void);							//LCD判忙



uchar Byte_Read(uint add);				   		//内部EEPROM字节读取
void Byte_Program(uint add, uchar ch);	  		//内部EEPROM字节写入
void Sector_Erase(uint add);			  		//内部EEPROM扇区擦除
void IAP_Disable();						  		//内部EEPROM功能关闭

void CheckHE(void);								//数据头尾校验
void CheckSum(void);							//数据求和校验
void CheckADD(void);							//地址校验
void BagPack(void);								//数据解包
void Reset_fun(void);							//复位
void Set_fun(void);								//设置
void Check_fun(void);							//查询
void Dbdata_fun(void);							//电表数据
void Linktest_fun(void);						//链路测试
void CheckT_fun(void);							//事件查询
//void Report_F(void);							//否认应答
void Report_T(void);						//确认应答

void Alarm_jude(void);							//预跳闸判断

void Init(void);								//初始化所有扇区数据
uchar Bcd2Hex(uchar b);							//BCD转HEX
uchar Hex2Bcd(uchar b);							//HEX转BCD

void Key_fun(void);								//按键处理函数

void Dis_Link(void);							//链路测试显示

void Dis_localadd(void);						//显示本机地址

void Dis_dbadd(void);							//显示电表表号
void Dis_LeaD(void);							//剩余电量
void Dis_LeaJ(void);						    //剩余金额
void Dis_DJ(void);								//电价
void Dis_Dqdls(void);							//当前电量示数
void Dis_Dqz(void);								//周期用电量
void Dis_GCount(void);							//总购电次数
void Dis_GTotal(void);							//累计购电量
void Dis_Tbuyj(void);							//累计购电金额
void Dis_Lastbuytime(void);						//最后一次购电时间
void Dis_Lastbuyj(void);						//最后一次购电金额 
void Dis_Nowjtdj(void);							//当前阶梯电价
void Dis_Lasttiming(void);						//上一次校时时间

void Alarm_reg(void);							//警报解除BY按键
void Alarm_Abo(void);							//警报解除BY充值


#endif

