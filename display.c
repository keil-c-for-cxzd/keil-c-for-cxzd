#include"config.h"
EXT_ uchar code ASCII[];
EXT_ uchar code ZM[];
/************判忙*************/
void LCD_Busy(void)
	{
	_nop_();
//	uchar Re;
//	do
//		{
//		A0=0;
//		RW=1;
//  		E1=1; 
//  		E2=0; 
//  		Re=DATA;
//		}
//	while((Re|0x80)==0x80);
//  	do
//		{
//		A0=0;
//		RW=1;
//		E1=0;
//  		E2=1;
//		Re=DATA;
//		}
//	while((Re|0x80)==0x80);
	}


/************写命令**************/

void LCD_Command(uchar nCommand)
	{
	LCD_Busy();                                 
  	A0=0;
	RW=0;
	delay(5);
  	E1=1; 
  	E2=1; 
  	DATA=nCommand;
	E1=0;
  	E2=0;	
	}

/************送显示数据**************/
void LCD_SetRam1(uchar ndata)
	{
	LCD_Busy();                
    A0=1; 
	RW=0;
	delay(5);
    E1=1;
    E2=0;  
    DATA=ndata;
    E1=0;
    E2=0;
	}

/************送显示数据**************/
void LCD_SetRam2(uchar ndata)
	{
	LCD_Busy();                
    A0=1; 
	RW=0;
    E1=0;
    E2=1;  
    DATA=ndata;
    E1=0;
    E2=0;
	}
/************LCD初始化**************/
void LCD_Init(void)
	{
	delay(1);
	LCD_Reset();  //复位
  	LCD_Command(LCD_DUTY32); //1/32
  	LCD_Command(0xa4);       //关闭静态显示    打开正常驱动
	LCD_Command(0xa0);       //列与段驱动反方向
  	LCD_EndRW();             //关闭自动列加1
  	LCD_SetStartLine(0);     //设置起始行
  	LCD_SetColumn(0);        //起始列
  	LCD_SetPage(0);          //起始页
  	LCD_ON();                //开显示
	}


/************清屏**************/
void clrscr(void)
	{
	uchar i;
	uchar page;
	for(page=0;page<4;page++)
  		{
  		LCD_SetPage(page);
   		for(i=0;i<61;i++)
     		{
     		LCD_SetColumn(i);
         	LCD_SetRam1(0x00);
			}
		for(i=0;i<61;i++)
     		{
     		LCD_SetColumn(i);
         	LCD_SetRam2(0x00);
			}
   		}
	}

/************LCD显示关闭**************/
void LCD_shutdown(void)
	{
	B_LED=1; 				 //关闭背光
  	LCD_OFF();               //关闭显示
	}

/************LCD显示开启**************/
void LCD_open(void)
	{
  	LCD_ON();                //开显示
	B_LED=0; 				 //开背光 
	}

/************文字显示**************/
void drawword(uchar liedizhi,uchar layer,uchar wenzi)
	{
	uchar column,x;//layer 值为0，2；wenzi 码表中的几个字
 	for(x=0;x<16;x++)
 		{
   		column=liedizhi+x;   
   		if(column>=61)
        	{
         	column-=61;
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer);
			LCD_SetRam2(ZM[wenzi*32+x]);
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer+1);
			LCD_SetRam2(ZM[wenzi*32+16+x]);
			}
   		else 
      		{
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer);
			LCD_SetRam1(ZM[wenzi*32+x]);
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer+1);
			LCD_SetRam1(ZM[wenzi*32+16+x]);    		
			}
 		}
	}

/************字母显示**************/
void drawASCII(uchar liedizhi,uchar layer,uchar wenzi)
	{
	uchar column,x;//layer 值为0，2；wenzi 码表中的几个字
 	for(x=0;x<8;x++)
 		{
   		column=liedizhi+x;   
   		if(column>=61)
        	{
         	column-=61;
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer);
			LCD_SetRam2(ASCII[wenzi*16+x]);
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer+1);
			LCD_SetRam2(ASCII[wenzi*16+8+x]);
			}
   		else 
      		{
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer);
			LCD_SetRam1(ASCII[wenzi*16+x]);
			LCD_SetColumn(column);//设置起始列
			LCD_SetPage(layer+1);
			LCD_SetRam1(ASCII[wenzi*16+8+x]);    		
			}
 		}
	}
/******************软件延迟*******************/

void delay(uchar nnn)
 {
  uchar mmm;
  while(nnn-->0)
  for(mmm=0;mmm<1;mmm++);
 }